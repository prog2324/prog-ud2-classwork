package prog.ud2.classwork;

public class Activitat12 {

    final static float CONVERSION_KM_MILLAS = 1.60934f;
    final static int SEGUNDOS_MINUTO = 60;
    final static int MINUTOS_HORA = 60;

    public static void main(String[] args) {
        float kmRecorridos = 14.5f;
        float minutos = 45;
        float segundos = 30;

        float minutosTotales = minutos + segundos / SEGUNDOS_MINUTO;
        System.out.println("DISTÀNCIA I TEMPS (km/h)");
        System.out.println("------------------------");
        System.out.printf("%.2f quilòmetres en %.0f' %.0f''\n\n", kmRecorridos, minutos, segundos);

        float millasRecorridas = kmRecorridos / CONVERSION_KM_MILLAS;
         /* Regla de 3:
            45,5 minutos -> millasRecorridas
            60 minutos   -> x
            X = 60 * millasRecorridas / 45.5
         */
        float mph = MINUTOS_HORA * millasRecorridas / minutosTotales;
        System.out.println("DISTÀNCIA I TEMPS (mph)");
        System.out.println("-----------------------");
        System.out.printf("%.2f milles en %d' \n", mph, MINUTOS_HORA);
    }
}
