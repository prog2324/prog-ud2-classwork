package prog.ud2.classwork;

/**
 *
 * @author sergio
 */
public class Activitat5 {

    public static void main(String[] args) {
        boolean A, B;

        System.out.println("A\tB\tA && B\tA || B\t!A");
        System.out.println("-------------------------------------");
        A = false;
        B = false;
        System.out.println(A + "\t" + B + "\t" + (A && B) + "\t" + (A || B) + "\t" + !A);

        A = true;
        B = false;
        System.out.println(A + "\t" + B + "\t" + (A && B) + "\t" + (A || B) + "\t" + !A);

        A = false;
        B = true;
        System.out.println(A + "\t" + B + "\t" + (A && B) + "\t" + (A || B) + "\t" + !A);

        A = true;
        B = true;
        System.out.println(A + "\t" + B + "\t" + (A && B) + "\t" + (A || B) + "\t" + !A);

    }

}
