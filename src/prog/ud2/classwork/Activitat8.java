package prog.ud2.classwork;

/**
 *
 * @author sergio
 */
public class Activitat8 {
    public static void main(String[] args) {
        int x = 150, i = 2000;
        
        System.out.println("Valor de x: " + x);
        System.out.println("Valor de i: " + i);
        System.out.println("Suma: " + (x+i));
        System.out.println("Resta: " + (x-i));
        System.out.println("Divisió: " + (x/i));
        System.out.println("Multiplicació: " + (x*i));
    }
}
