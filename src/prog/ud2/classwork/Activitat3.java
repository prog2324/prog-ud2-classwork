package prog.ud2.classwork;

/*
Activitat3. Crea un programa que declare 4 variables, 
assigna'ls el teu nom, cognoms, edat i si estàs casat o no. 
Seguidament mostra el seu contingut per pantalla.​
 */
public class Activitat3 {
    
    public static void main(String[] args) {
        String nom;
        String cognoms;
        byte edat;
        boolean casado;
        nom = "Sergio";
        cognoms = "Galisteo Castro";
        edat = 20;
        casado = false;
        
        System.out.println("Me llamo: " + nom + " " + cognoms);
        System.out.println("Tengo " + edat + " años");
        System.out.println("Y estado civil casado: "+casado);
        
    }    
}
