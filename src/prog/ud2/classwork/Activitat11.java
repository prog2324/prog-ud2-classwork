package prog.ud2.classwork;

import java.util.Scanner;

/*
Activitat 11. Implementa un programa que demane a l’usuari el número de monedes que té de: 
2 cèntims, 5 cèntims, 10 cèntims, 20 cèntims, 50 cèntims, 1 euro y 2 euros. 
Al finalitzar ha de mostrar quants diners té en euros.​
 */
public class Activitat11 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.print("Quantes monedes tens de 2 cèntims? ");
        int dosCentims = entrada.nextInt();
        System.out.print("Quantes monedes tens de 5 cèntims? ");
        int cincCentims = entrada.nextInt();
        System.out.print("Quantes monedes tens de 10 cèntims? ");
        int deuCentims = entrada.nextInt();
        System.out.print("Quantes monedes tens de 20 cèntims? ");
        int vintCentims = entrada.nextInt();
        System.out.print("Quantes monedes tens de 50 cèntims? ");
        int cinquantaCentims = entrada.nextInt();
        System.out.print("Quantes monedes tens de 1€? ");
        int unEuro = entrada.nextInt();
        System.out.print("Quantes monedes tens de 2€? ");
        int dosEuros = entrada.nextInt();

        int totalCentims = dosCentims * 2 + cincCentims * 5 + deuCentims * 10 + vintCentims * 20;
        totalCentims += cinquantaCentims * 50 + unEuro * 100 + dosEuros * 200;
        
        int euros = totalCentims / 100;
        int centims = totalCentims % 100;
        
        System.out.printf("En total tens %d € y %d cèntims \n", euros, centims);

    }
}
