package prog.ud2.classwork;

/**
 *
 * @author sergio
 */
public class Activitat9 {
    final static short RATIO_CONVERSIO = 1024;
    
    public static void main(String[] args) {
        // final short RATIO_CONVERSIO = 1024;
        int v1 = 40000, v2 = 36000;
        
        System.out.println(v1 + " Mb equivalen a " + v1 * RATIO_CONVERSIO + " Kb");
        System.out.println(v2 + " Mb equivalen a " + v2 * RATIO_CONVERSIO + " Kb");
        
    }
}
