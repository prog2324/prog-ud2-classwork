package prog.ud2.classwork;

import java.util.Scanner;

/**
 *
 * @author sergio
 */
public class Activitat10 {
    public static void main(String[] args) {
         double a, b, c;
        
        Scanner teclat = new Scanner(System.in);
        System.out.print("Dame valor para variable a: ");
        a = teclat.nextDouble();
        
        System.out.print("Dame valor para variable b: ");
        b = teclat.nextDouble();
        
        System.out.print("Dame valor para variable c: ");
        c = teclat.nextDouble();
        
        System.out.println("Resultat multiplicació: " + (a * b * c));
        
        teclat.close();
        
    }
}
